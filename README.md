# Project Title

A paragraph of project description

## Quick Start

Very quick installing and usage method

## Prerequisites

 * Package 1
 * Package 2

```
If the source contains a method to install all prerequisites, add here.
Example: pip install -r requirements.txt
```

## Installing

Step-by-step guide to installing 
```
Steps
```

### Testing

Guide on how to run an automated test here


## Deployment

If this source is deployable, explain here


## Versioning

[SemVer](http://semver.org/) versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Billie Thompson** - *Initial work* - [PurpleBooth](https://github.com/PurpleBooth)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the {} License - see the [LICENSE.md](LICENSE.md)

## Acknowledgments

* Name 1 (email1)
* Name 2 (email2)
* Name 3 (email3)

