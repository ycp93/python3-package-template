#!/usr/bin/env python3

from setuptools import setup, find_packages

# https://packaging.python.org/tutorials/packaging-projects/
with open("README.md", "r") as f:
    long_description = f.read()

setup(
    name = "PackageName",
    version = "0.9.0",
    install_requires = ['numpy', 'scipy', 'click'],
    include_package_data = True,
    py_modules = [],
    entry_points = {
        'console_scripts': [
            'mycommand = package.exe:cli',
        ],
    },
    
# Metadata
    author = "Young-Chan Park",
    author_email = "young.chan.park93@gmail.com",
    description = "",
    long_description = long_description,
    long_description_content_type = "text/markdown",
    url = "https://www.packagename.com",
    packages = find_packages(),
    classifiers = [
        "Programming Language :: Python :: 3",
        "Operating System :: CentOS 7.6"
        "License :: OSI Approved :: Python Software Foundation License"
    ],
    python_requires='>=3.6',
)
